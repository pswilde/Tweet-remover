###
### THIS SCRIPT WILL DELETE ALL YOUR TWITTER POSTS
### ONLY USE THIS SCRIPT IF THAT IS WHAT YOU WANT TO DO!!!!!!!!
###

##
## Name: Tweet Remover
## Author: Paul Wilde
## Date: 26 Apr 2022
##

import tweepy

# put your api auth details here:
key = "user key" 
sec = "user secret"
tok = "token"
tok_sec = "token secret"

# create OAuth Handler
auth = tweepy.OAuthHandler(key, sec)
auth.set_access_token(tok, tok_sec)

# Authorise with API
api = tweepy.API(auth)

# function to delete 
def go_delete(api):
    # find tweets in user time line
    tweets = api.user_timeline()
    # for each tweet, delete it
    for tweet in tweets:
        print("Deleting " + tweet.id)
        print(tweet.text)
        api.destroy_status(tweet.id)


# we can only delete so many tweets in a single connection,
# so we'll loop until we're complete
# it'll probably crash when everything's gone, but who cares :-)
while True:
    go_delete(api)
